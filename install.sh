#!/usr/bin/env bash
#set -x

pushd `pwd`
cd `dirname $0`
if [ ! -d "bundle/vundle" ]
then
    git clone https://github.com/gmarik/vundle.git bundle/vundle
fi
echo "set nocompatible" > install.rc
echo "set rtp+=~/.vim/bundle/vundle/" >> install.rc
echo "call vundle#rc()" >> install.rc

fgrep Bundle vimrc  | egrep -v -e '^"' >> install.rc
vim -u ./install.rc +BundleInstall +qall
rm -f install.rc

popd

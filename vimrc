" vim: set sw=4 ts=4 sts=4 et tw=78 foldmarker={,} foldlevel=0 foldmethod=marker spell:
" ==========================================================
" Dependencies - Libraries/Applications outside of vim
" ==========================================================





" ==========================================================
" Shortcuts
" ==========================================================
nnoremap <silent> <leader>f :Unite  file<CR>kA
set nocompatible              " Don't be compatible with vi
let mapleader=","             " change the leader to be a comma vs slash



" Seriously, guys. It's not like :W is bound to anything anyway.
command! W :w

" Vundle - Allows us to organize our vim plugins
" ==========================================================
" Load all vundle

if isdirectory("/localdisk/a_colson/.vim/bundle/")
    set rtp+=/localdisk/a_colson/.vim/bundle/vundle/
    call vundle#rc("/localdisk/a_colson/.vim/bundle/")
else
    set rtp+=~/.vim/bundle/vundle/
    call vundle#rc()
endif


" ==========================================================
" Plugins included 
" ==========================================================
"{
"Bundle 'vim-scripts/The-NERD-tree'
"Bundle 'ervandew/supertab'
"Bundle 'sjl/gundo.vim'
"Bundle 'vim-scripts/pep8'
Bundle 'vim-scripts/a.vim'
Bundle 'Lokaltog/vim-powerline'
"Bundle 'Raimondi/delimitMate'
Bundle 'terryma/vim-multiple-cursors'
Bundle 'flazz/vim-colorschemes'
Bundle 'tpope/vim-unimpaired'
Bundle 'tpope/vim-fugitive'
Bundle 'tpope/vim-git'
Bundle 'tpope/vim-surround'
Bundle 'tpope/vim-abolish'
Bundle 'tpope/vim-repeat'
Bundle 'tpope/vim-commentary'
Bundle 'godlygeek/tabular'  
"Bundle 'Shougo/vimshell'
Bundle 'kien/ctrlp.vim'
Bundle 'Shougo/neomru.vim'
Bundle 'Shougo/vimproc.vim'
Bundle 'Shougo/neocomplcache'
if  has("lua")
    Bundle 'Shougo/neocomplete'
endif
if v:version > 702
    Bundle 'Shougo/unite.vim'
endif

Bundle 'MarcWeber/vim-addon-mw-utils'
Bundle 'tomtom/tlib_vim'
Bundle 'garbas/vim-snipmate'
" Optional:
Bundle 'honza/vim-snippets'
if v:version > 703
    Bundle 'chrisbra/vim-diff-enhanced'
endif

"endif
"if exists('clang')
"    Bundle 'Rip-Rip/clang_complete'
"endif
"}

" ==========================================================
" Basic Settings
" ==========================================================
"{
"H
" Bubble single lines
nmap <C-Up> [e
nmap <C-Down> ]e
" Bubble multiple lines
vmap <C-Up> [egv
vmap <C-Down> ]egv

" Buffer resizing 
nmap <S-Up> :resize +1<CR>
nmap <S-Down> :resize -1<CR>
nmap <F12> :1winc< <CR>
nmap <F11> :1winc> <CR>

" tab navigation


" no more visual mode
nnoremap Q <nop>

" for linux and windows users (using the control key)
map <C-S-]> gt
map <C-S-[> gT
map <C-1> 1gt
map <C-2> 2gt
map <C-3> 3gt
map <C-4> 4gt
map <C-5> 5gt
map <C-6> 6gt
map <C-7> 7gt
map <C-8> 8gt
map <C-9> 9gt
map <C-0> :tablast<CR>


syntax on                     " syntax highlighing
filetype on                   " try to detect filetypes
filetype plugin indent on     " enable loading indent file for filetype
set number                    " Display line numbers
set numberwidth=1             " using only 1 column (and 1 space) while possible
set background=dark           " We are using dark background in vim
set title                     " show title in console title bar
set wildmenu                  " Menu completion in command mode on <Tab>
set wildmode=full             " <Tab> cycles between all matching choices.


""" Moving Around/Editing {
set cursorline              " have a line indicate the cursor location
set ruler                   " show the cursor position all the time
set nostartofline           " Avoid moving cursor to BOL when jumping around
set virtualedit=block       " Let cursor move past the last char in <C-v> mode
set scrolloff=3             " Keep 3 context lines above and below the cursor
set backspace=2             " Allow backspacing over autoindent, EOL, and BOL
set showmatch               " Briefly jump to a paren once it's balanced
set nowrap                  " don't wrap text
set linebreak               " don't wrap textin the middle of a word
set autoindent              " always set autoindenting on
set smartindent             " use smart indent if there is no indent file
set tabstop=2               " <tab> inserts 4 spaces 
set shiftwidth=2            " but an indent level is 2 spaces wide.
set softtabstop=2           " <BS> over an autoindent deletes both spaces.
set expandtab               " Use spaces, not tabs, for autoindent/tab key.
set shiftround              " rounds indent to a multiple of shiftwidth
set matchpairs+=<:>         " show matching <> (html mainly) as well
set foldmethod=indent       " allow us to fold on indents
set foldlevel=99            " don't fold by default
set nu
"set mouse=a                 " Automatically enable mouse
"set mousehide               " Hide mouse cursor while typing"

"}
if has("mouse_sgr") && v:version > 703
    set ttymouse=sgr
else
    set ttymouse=xterm2
end

"""" Reading/Writing {
set noautowrite             " Never write a file unless I request it.
set noautowriteall          " NEVER.
set noautoread              " Don't automatically re-read changed files.
set modeline                " Allow vim options to be embedded in files;
set modelines=5             " they must be within the first or last 5 lines.
set ffs=unix,dos,mac        " Try recognizing dos, unix, and mac line endings.
set noswapfile
"}

"""" Messages, Info, Status {
set ls=2                    " allways show status line
set vb t_vb=                " Disable all bells.  I hate ringing/flashing.
set confirm                 " Y-N-C prompt if closing with unsaved changes.
set showcmd                 " Show incomplete normal mode commands as I type.
set report=0                " : commands always print changed line count.
set shortmess+=a            " Use [+]/[RO]/[w] for modified/readonly/written.
set ruler                   " Show some info, even without statuslines.
set laststatus=2            " Always show statusline, even if only 1 window.
"set statusline=[%l,%v\ %P%M]\ %f\ %r%h%w\ (%{&ff})\ %{fugitive#statusline()} 
if has('statusline')
    set laststatus=2
    " Broken down into easily includeable segments
    set statusline=%<%f\                     " Filename
    set statusline+=%w%h%m%r                 " Options
    "set statusline+=%{fugitive#statusline()} " Git Hotness
    set statusline+=\ [%{&ff}/%Y]            " Filetype
    set statusline+=\ [%{getcwd()}]          " Current dir
    set statusline+=%=%-14.(%l,%c%V%)\ %p%%  " Right aligned file nav info
endif
set showmode
set mouse=a
"}

"""" Always splits to the right and below
set splitright
set splitbelow

"""" Improve search display
set hlsearch
map <leader>h :let @/ = '\<'.expand('<cword>').'\>'\|set hlsearch<C-M>
set incsearch


" Ignore these files when completing
set wildignore+=*.o,*.obj,.git,*.pyc    


"""" Change update time (for augroup)
set updatetime=250

"}

" Remapping {
map <leader>c <C-w><C-q>

" <|>: Reselect visual block after indent
vnoremap < <gv
vnoremap > >gv

" Yank from the cursor to the end of the line, to be consistent with C and D.
nnoremap Y y$

map <leader>p :set paste!<CR>


" Code folding options  {
"nmap <leader>f0 :set foldlevel=0<CR>
"nmap <leader>f1 :set foldlevel=1<CR>
"nmap <leader>f2 :set foldlevel=2<CR>
"nmap <leader>f3 :set foldlevel=3<CR>
"nmap <leader>f4 :set foldlevel=4<CR>
"nmap <leader>f5 :set foldlevel=5<CR>
"nmap <leader>f6 :set foldlevel=6<CR>
"nmap <leader>f7 :set foldlevel=7<CR>
"nmap <leader>f8 :set foldlevel=8<CR>
"nmap <leader>f9 :set foldlevel=9<CR>
"}
" Some helpers to edit mode
" http://vimcasts.org/e/14
cnoremap %% <C-R>=expand('%:h').'/'<cr>
map <leader>ew :e %%
map <leader>es :sp %%
map <leader>ev :vsp %%
map <leader>et :tabe %%
map <leader>q :qa<CR>

" Map <Leader>ff to display all lines with keyword under cursor
" and ask which one to jump to
nmap <Leader>ff [I:let nr = input("Which one: ")<Bar>exe "normal " . nr ."[\t"<CR>


"}
let g:solarized_termcolors=256

"""" Display {
if has("gui_running")
     colorscheme molokai
     "Remove menu bar
     set guioptions-=m
     " Remove toolbar
     set guioptions-=T
else
    colorscheme molokai
endif
"}

"""""""""""""""""""""""""""""
" Plugin specific configs
"""""""""""""""""""""""""""""
"{

" Tabularize {
nmap <Leader>a& :Tabularize /&<CR>
vmap <Leader>a& :Tabularize /&<CR>
nmap <Leader>a= :Tabularize /=<CR>
vmap <Leader>a= :Tabularize /=<CR>
nmap <Leader>a: :Tabularize /:<CR>
vmap <Leader>a: :Tabularize /:<CR>
nmap <Leader>a:: :Tabularize /:\zs<CR>
vmap <Leader>a:: :Tabularize /:\zs<CR>
nmap <Leader>a, :Tabularize /,<CR>
vmap <Leader>a, :Tabularize /,<CR>
nmap <Leader>a,, :Tabularize /,\zs<CR>
vmap <Leader>a,, :Tabularize /,\zs<CR>
nmap <Leader>a<Bar> :Tabularize /<Bar><CR>
vmap <Leader>a<Bar> :Tabularize /<Bar><CR>
" }

" buffer switching shortcut
map <leader>, <Esc>:AV<CR>
imap <Leader>n <ESC>:IHS<CR>:A<CR>
nmap <Leader>n :IHS<CR>:A<CR>

"power line 
"set fillchars+=stl:\ ,stlnc:\
"set encoding=utf-8
"let g:Powerline_symbols = 'fancy'
"set laststatus=2 

"multiple cursors {
let g:multi_cursor_use_default_mapping=1
let g:multi_cursor_next_key='<C-d>'


" Unite {

if v:version > 702
    " Use the fuzzy matcher for everything
    "call unite#filters#matcher_default#use(['matcher_fuzzy'])

    " Use the rank sorter for everything
    call unite#filters#sorter_default#use(['sorter_rank'])
    call unite#custom#profile('source/grep', 'context' , {'no_quit': 1})
    call unite#custom#profile('file_rec,file_rec/async,file_mru,file,buffer,grep', 'context.ignorecase' , 1 )
    call unite#custom#profile('file_rec/async', 'context.ignorecase' , 1 )
    let g:unite_source_grep_command='find'
    "let g:unite_source_grep_command='fgrep'
    let g:unite_source_grep_default_opts='. -regex ".*\.\(h\|c\|cu\|H\|C\|hh\|cc\|hpp\|cpp\|hxx\|cxx\|py\|pyx\|inl\|moc\|java\|sql\|params\)\|.*akefile\|.*cmake.*" -print0 | xargs -0 fgrep --color -n'
    "let g:unite_source_grep_default_opts=' --exclude=".git" --exclude="build" --exclude=tags -inH'
    " Set up some custom ignores
    call unite#custom_source('file_rec,file_rec/async,file_mru,file,buffer,grep', 'ignore_pattern', join([ '\.git/*', '.*\.pyc' ,'build/'], '\|'))

    " NEW
    " CtrlP search
    " replacing unite with ctrl-p
    nnoremap <silent> <leader>r :Unite -start-insert -buffer-name=files -winheight=10 file_rec/async<cr>    
endif
"}

" Fugitive {
nnoremap <silent> <leader>gs :Gstatus<CR>
nnoremap <silent> <leader>gd :Gdiff<CR>
nnoremap <silent> <leader>gc :Gcommit<CR>
nnoremap <silent> <leader>gb :Gblame<CR>
nnoremap <silent> <leader>gl :Glog<CR>
nnoremap <silent> <leader>gp :Git push<CR>
nnoremap <silent> <leader>gr :Gread<CR>
nnoremap <silent> <leader>gw :Gwrite<CR>
nnoremap <silent> <leader>ge :Gedit<CR>
" Mnemonic _i_nteractive
nnoremap <silent> <leader>gi :Git add -p %<CR>
nnoremap <silent> <leader>gg :SignifyToggle<CR>
"}

" CTags {
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
" " }
"

" Neocache Complete
let g:neocomplcache_enable_at_startup = 1
" Use smartcase.
let g:neocomplcache_enable_smart_case = 1
" Set minimum syntax keyword length.
"}
"
function! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\v\s+$//e
    call cursor(l, c)
endfun

" Using file extension
" autocmd BufWritePre *.h,*.c,*.java,*.C,*.py,*.cpp :call <SID>StripTrailingWhitespaces()
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/


au FileType python setl sw=4 ts=4 sts=4 

